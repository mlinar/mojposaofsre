var express = require('express');
var mainController = require('./controllers/mainController');
var session = require('express-session');
const app = express();

//setting up template engine
app.set('view engine', 'ejs');

//static files
app.use(express.static('./assets'));
app.use(session({secret:"asdfghjkl", resave:true, saveUninitialized:true}));
//fire controllers
mainController(app);

//listen to port
app.listen(4001, () => console.log('App listening on port 4000!'));
