var mysql = require('mysql');
var bcrypt = require('bcrypt');
var config = require('../configDB');
var con = mysql.createConnection(config.database);
module.exports = function(req, res){
  if(req.body.lozinka === req.body.lozinkaPon){
    con.connect(function(err) {
    if (err) {console.log("Greska")};
    console.log("Spojen na bazu");
    con.query("SELECT COUNT(*) AS Count FROM korisnik WHERE email=?;",[req.body.email], function (err, result, fields) {
      if (err) console.log(err);
      if(result[0].Count === 0){
        con.query("SELECT COUNT(*) AS Count FROM tvrtka WHERE email=?;",[req.body.email], function (err, result, fields) {
          if (err) console.log(err);
          if(result[0].Count === 0){
            var insert  = {ime: req.body.ime, prezime: req.body.prezime, email: req.body.email};
            var sql = "INSERT INTO korisnik SET ?";
            con.query(sql, insert, function (err, result) {
              if (err) console.log(err);
              bcrypt.hash(req.body.lozinka, 10, function(err, hash) {
                var sql = "UPDATE korisnik SET lozinka = ? WHERE email = ?";
                con.query(sql,[hash, req.body.email],function (err, result) {
                  if (err) console.log(err);
                  req.session.podaci = {
                    ime: req.body.ime,
                    email: req.body.email,
                    korisnik: true
                  };
                  res.redirect('/');
                });
              });
              console.log("1 korisnik inserted");
            });
          }
        });
      }
      else{
        res.redirect('back');
        console.log("Korisnik sa email-om: " + req.body.email + " već postoji u bazi.");
      }
    });
  });
  }
}
