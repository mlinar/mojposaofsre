var bodyParser = require('body-parser');
var regKorisnikController = require('./regKorisnikController');
var regTvrtkaController = require('./regTvrtkaController');
var loginController = require('./loginController');

module.exports = function(app){
  app.get('/', function(req, res){
    if(!req.session.podaci){return res.render('index');}

    return res.render('index', {
    layout:false,
    session: req.session.podaci
    });
  });
  app.get('/natjecaji', function(req,res){
    if(!req.session.podaci)return res.render('natjecaji');

    return res.render('natjecaji', {
    layout:false,
    session: req.session.podaci
    });
  });
  app.get('/adminPanel', function(req, res){
    if(!req.session.podaci.admin){
      session = null;
      return res.sendStatus(404);
    }
    res.render('admin');
  });
  app.get('/myProfile', function(req, res){
    res.render('myProfile', {
    layout:false,
    session: req.session.podaci
    });
    console.log(req.session);
  });
  app.get('/login', function(req, res){
    res.render('login');
  });
  app.get('/logout', function(req,res){
    req.session.destroy();
    res.render('index');
  });
  app.get('/registracija', function(req, res){
    res.render('registracija');
  });
  var urlencodedParser = bodyParser.urlencoded({ extended: false });
  app.post('/registerKorisnik', urlencodedParser, function(req, res){
    if (!req.body) return res.sendStatus(400);
    regKorisnikController(req, res);
  });
  app.post('/registerTvrtka', urlencodedParser, function(req, res){
    if (!req.body) return res.sendStatus(400);
    regTvrtkaController(req, res);
  });

  app.post('/logIn', urlencodedParser, function (req, res) {
  if (!req.body) return res.sendStatus(400);
  loginController(req, res);
});
};
