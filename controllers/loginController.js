var mysql = require('mysql');
var config = require('../configDB');
var con = mysql.createConnection(config.database);

module.exports = function(req, res){
  con.connect(function(err) {
  if (err) {console.log("Greska")};
  console.log("Spojen na bazu");
  con.query("SELECT COUNT(*) AS Count FROM korisnik WHERE email=?;",[req.body.email], function (err, result, fields) {
    if (err) console.log(err);
    if(result[0].Count === 0){
      con.query("SELECT COUNT(*) AS Count FROM tvrtka WHERE email=?;",[req.body.email], function (err, result, fields) {
        if (err) console.log(err);
        if(result[0].Count === 0){
          console.log("Nema korisnika u bazi.");
        }
        else{
          console.log("Tvrtka");
          con.query("SELECT * FROM tvrtka WHERE email=?;",[req.body.email], function (err, result, fields) {
            if (err) console.log(err);
            var naziv = result[0].naziv;
            req.session.podaci = {
              ime: naziv
            };
            console.log(naziv);
            console.log(req.session.podaci.ime);
            res.redirect('/');
          });
        }
      });
    }
    else{
      console.log("Korisnik");
      con.query("SELECT * FROM korisnik WHERE email=?;",[req.body.email], function (err, result, fields) {
        if (err) console.log(err);
        var ime = result[0].ime;
        var korisnik = false;
        var admin = true;
        if (req.body.email === "admin@admin.com"){
           admin = true;
           req.session.podaci = {
             admin: admin
           };
           res.redirect('/adminPanel');
        }
        else{
          korisnik = true;
          req.session.podaci = {
            ime: ime,
            email: req.body.email,
            korisnik: korisnik
          };
          console.log(ime);
          console.log(req.session.podaci.ime);
          res.redirect('/');
        }
      });
    }
  });
});
}
